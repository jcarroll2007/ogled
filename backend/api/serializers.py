from django.contrib.auth import get_user_model
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    new_password = serializers.CharField(max_length=50, allow_null=True, write_only=True, required=False)

    class Meta:
        model = get_user_model()
        fields = ('id', 'first_name', 'last_name', 'new_password', 'email')

    def update(self, instance, validated_data):
        user = super(UserSerializer, self).update(instance, validated_data)
        if 'new_password' in validated_data and validated_data['new_password'] is not None:
            user.set_password(validated_data['new_password'])
            user.save()
        return user

    def create(self, validated_data):
        new_password = validated_data.pop('new_password', False)
        if new_password and new_password is not None:
            user = get_user_model().objects.create_user(**validated_data)
            user.set_password(new_password)
            user.save()
            return user
