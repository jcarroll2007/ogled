from django.contrib.auth import logout as django_logout
from django.contrib.auth import login as django_login
from django.contrib.auth import authenticate

from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status

from api.serializers import UserSerializer
from django.contrib.auth import get_user_model


@api_view(["POST"])
@permission_classes(())
def login(request):
    username = request.data.get('username', None)
    password = request.data.get('password', None)

    if username is None or password is None:
        return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                         'detail': 'Credentials not provided.'}, status=status.HTTP_400_BAD_REQUEST)

    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            django_login(request, user)
            return Response(UserSerializer(user).data)
    return Response({'status_code': status.HTTP_404_NOT_FOUND,
                     'detail': 'Provided credentials did not authenticate. Please try again.'}, status=status.HTTP_404_NOT_FOUND)

@api_view(["GET"])
def logout(request):
    django_logout(request)
    return Response({})

@api_view(["POST"])
@permission_classes(())
def register(request):
    first_name = request.data.get('first_name')
    last_name = request.data.get('last_name')
    email = request.data.get('email')
    password = request.data.get('password')

    user = get_user_model().objects.create_user(
        first_name=first_name,
        last_name=last_name,
        email=email,
        password=password)
    return Response(UserSerializer(user).data)
