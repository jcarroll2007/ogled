from rest_framework.views import exception_handler
from rest_framework import status

def status_code_handler(exc, context):
    response = exception_handler(exc, context)

    if response is not None:
        response.data['status_code'] = response.status_code

        if response.status_code == status.HTTP_403_FORBIDDEN and response.data['detail'] == 'Authentication credentials were not provided.':
            response.data['status_code'] = status.HTTP_401_UNAUTHORIZED
            response.status_code = status.HTTP_401_UNAUTHORIZED
            response.reason_phrase = 'UNAUTHORIZED'

    return response
