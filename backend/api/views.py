from django.contrib.auth import get_user_model

from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

from api.serializers import UserSerializer

from listings.models import Group, Category, SubCategory


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer

    @list_route()
    def current(self, request):
        return Response(UserSerializer(request.user).data)

    def create(self, request):
    	return Response('test')
