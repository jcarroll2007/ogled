from django.conf.urls import url, include

from rest_framework import routers

from api import views
from api import auth
from listings.views import groups, ListingViewSet, PeriodViewSet

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'listings', ListingViewSet)
router.register(r'periods', PeriodViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^login/', auth.login),
    url(r'^logout/', auth.logout),
    url(r'^register/', auth.register),
    url(r'^groups/', groups),
]
