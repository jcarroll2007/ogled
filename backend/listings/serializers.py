from rest_framework import serializers
from models import Group, Category, SubCategory, Listing, Period


class ListingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Listing

class PeriodSerializer(serializers.ModelSerializer):
    class Meta:
        model = Period
        fields = ('id', 'name', 'start', 'end')

class SubCategorySerializer(serializers.ModelSerializer):
    listings = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = SubCategory
        fields = ('id', 'name', 'listings')

class CategorySerializer(serializers.ModelSerializer):
    sub_categories = SubCategorySerializer(many=True, read_only=True)
    listings = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ('id', 'name', 'sub_categories', 'listings')

class GroupSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(many=True, read_only=True)
    listings = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Group
        fields = ('id', 'name', 'categories', 'listings')
