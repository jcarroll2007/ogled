# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
            ],
            options={
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='Creator',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('bio', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
            ],
            options={
                'verbose_name_plural': 'Groups',
            },
        ),
        migrations.CreateModel(
            name='Listing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300)),
                ('price', models.DecimalField(max_digits=12, decimal_places=2)),
                ('description', models.TextField()),
                ('category', models.ForeignKey(related_name='listings', blank=True, to='listings.Category', null=True)),
                ('creator', models.ForeignKey(related_name='listings', to='listings.Creator')),
                ('group', models.ForeignKey(related_name='listings', to='listings.Group')),
            ],
        ),
        migrations.CreateModel(
            name='Period',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('start', models.IntegerField(max_length=4)),
                ('end', models.IntegerField(max_length=4)),
            ],
        ),
        migrations.CreateModel(
            name='SubCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150)),
                ('category', models.ForeignKey(related_name='sub_categories', to='listings.Category')),
            ],
            options={
                'verbose_name_plural': 'Sub Categories',
            },
        ),
        migrations.AddField(
            model_name='listing',
            name='period',
            field=models.ForeignKey(related_name='listings', to='listings.Period'),
        ),
        migrations.AddField(
            model_name='listing',
            name='sub_category',
            field=models.ForeignKey(related_name='listings', blank=True, to='listings.SubCategory', null=True),
        ),
        migrations.AddField(
            model_name='category',
            name='group',
            field=models.ForeignKey(related_name='categories', to='listings.Group'),
        ),
    ]
