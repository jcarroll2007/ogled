from django.contrib import admin
from models import Creator, Period, Group, Category, SubCategory, Listing

# Register your models here.
admin.site.register(Creator)
admin.site.register(Period)
admin.site.register(Group)
admin.site.register(Category)
admin.site.register(SubCategory)
admin.site.register(Listing)
