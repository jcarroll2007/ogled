from django.db import models

class Creator(models.Model):
    name = models.CharField(max_length=150)
    bio = models.TextField()

    def __str__(self):
        return self.name

class Period(models.Model):
    name = models.CharField(max_length=150)
    start = models.IntegerField(max_length=4)
    end = models.IntegerField(max_length=4)

    def __str__(self):
        return self.name + ' | ' + str(self.start) + '-' + str(self.end)

class Group(models.Model):
    name = models.CharField(max_length=150)

    class Meta:
        verbose_name_plural = 'Groups'

    def __str__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length=150)
    group = models.ForeignKey(Group, related_name='categories')

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return str(self.group) + ' | ' + self.name

class SubCategory(models.Model):
    name = models.CharField(max_length=150)
    category = models.ForeignKey(Category, related_name='sub_categories')

    class Meta:
        verbose_name_plural = 'Sub Categories'

    def __str__(self):
        return str(self.category) + ' | ' + self.name

class Listing(models.Model):
    title = models.CharField(max_length=300)
    price = models.DecimalField(max_digits=12, decimal_places=2)
    description = models.TextField()
    period = models.ForeignKey(Period, related_name='listings')
    creator = models.ForeignKey(Creator, related_name='listings')
    group = models.ForeignKey(Group, related_name='listings')
    category = models.ForeignKey(Category, related_name='listings', null=True, blank=True)
    sub_category = models.ForeignKey(SubCategory, related_name='listings', null=True, blank=True)

    def __str__(self):
        return self.creator.name + ' | ' + self.title
