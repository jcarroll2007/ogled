from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status
from rest_framework.filters import DjangoFilterBackend

from serializers import GroupSerializer, CategorySerializer, SubCategorySerializer, ListingSerializer, PeriodSerializer
from models import Group, Category, SubCategory, Listing, Period

class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class SubCategoryViewSet(viewsets.ModelViewSet):
    queryset = SubCategory.objects.all()
    serializer_class = SubCategorySerializer

class ListingViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Listing.objects.all()
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['group', 'category', 'sub_category']
    serializer_class = ListingSerializer
    permission_classes = []

class PeriodViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Period.objects.all()
    serializer_class = PeriodSerializer
    permission_classes = []

@api_view(["GET"])
@permission_classes(())
def groups(request):
    groups = Group.objects.all()
    return Response(GroupSerializer(groups, many=True).data)
