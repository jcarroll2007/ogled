from django.conf.urls import include, url
from django.contrib import admin

import api.urls

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(api.urls)),
]
