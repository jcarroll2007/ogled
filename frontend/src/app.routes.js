(function () {
    'use strict';

    function config($stateProvider, $urlRouterProvider) {

        ///////////////////////////
        // Redirects and Otherwise
        ///////////////////////////

        $urlRouterProvider
            .otherwise('/');

        ///////////////////////////
        // State Configurations
        ///////////////////////////

        $stateProvider
            .state('app', {
                abstract: true,
                templateUrl: 'app/app.html'
            })

            ///////////////////////////
            // Home
            ///////////////////////////
            .state('app.home', {
                url: '/',
                template: '<home></home>'
            })

            ///////////////////////////
            // Home
            ///////////////////////////
            .state('app.listings', {
                url: '/listings?group&category&sub_category',
                template: '<listings ng-model="listings"></listings>',
                resolve: {
                    listings: function (Listings, $stateParams) {
                        return Listings.getList($stateParams)
                            .then(function (listings) {
                                return listings;
                            });
                    }
                },
                controller: function ($scope, listings) {
                    $scope.listings = listings;
                }
            })

            ///////////////////////////
            // About
            ///////////////////////////
            .state('app.about', {
                url: 'about',
                template: '<about></about>'
            })

            ///////////////////////////
            // Contact
            ///////////////////////////
            .state('app.contact', {
                url: 'contact',
                template: '<contact></contact>'
            });
    }

    angular.module('app.routes', [])
        .config(config);
}());