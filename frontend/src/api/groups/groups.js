(function () {
    'use strict';

    function Groups(url, $http) {
        var api = {};
        api.name = 'users';

        api.get = function () {
            return $http.get(url('groups'));
        };

        return api;
    }

    angular.module('api.groups', [])
        .factory('Groups', Groups);
}());