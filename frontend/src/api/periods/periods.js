(function () {
    'use strict';

    function Periods(url, $http) {
        var periods = {};

        periods.getList = function (params) {
            return $http.get(url('periods'), {
                params: params
            })
                .then(function (response) {
                    return response.data;
                });
        };

        return periods;
    }

    angular.module('api.periods', [])
        .factory('Periods', Periods);
}());