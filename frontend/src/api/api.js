(function () {
    'use strict';

    function url() {
        return function (extension) {
            var base = '/api/';
            return base + extension + '/';
        };
    }

    function apiConfig($httpProvider) {
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    }

    angular.module('api', [
        'api.users',
        'api.listings',
        'api.categories',
        'api.groups',
        'api.periods'
    ])
        .factory('url', url)
        .config(apiConfig);
}());