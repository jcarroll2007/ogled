(function () {
    'use strict';

    function Users(url, $http, $q) {
        var api = {};
        api.name = 'users';

        api.register = function (first_name, last_name, email, password, password_verification) {
            if (password !== password_verification) {
                return $q.reject('Passwords do not match.');
            }
            return $http.post(url('users'), {
                first_name: first_name,
                last_name: last_name,
                email: email,
                new_password: password
            });
        };

        api.login = function (email, password) {
            return $http.post(url('login'), {username: email, password: password});
        };
        // api.getCurrent()

        return api;
    }

    angular.module('api.users', [])
        .factory('Users', Users);
}());