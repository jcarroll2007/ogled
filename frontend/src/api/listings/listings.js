(function () {
    'use strict';

    function Listings(url, $http) {
        var listings = {};

        listings.getList = function (params) {
            return $http.get(url('listings'), {
                params: params
            })
                .then(function (response) {
                    return response.data;
                });
        };

        return listings;
    }

    angular.module('api.listings', [])
        .factory('Listings', Listings);
}());