(function () {
    'use strict';
    var tables,
        desks,
        bookcases,
        furniture,
        candle,
        centerpieces,
        silver,
        clocks,
        categoryFixtureData;

    /**
     * Furniture
     */

    tables = {
        id: 1,
        name: 'Tables',
        children: [{
            id: 1,
            name: 'Dining Tables',
            listings: []
        }, {
            id: 2,
            name: 'Occasional Tables',
            listings: []
        }, {
            id: 3,
            name: 'Breakfast Tables',
            listings: []
        }, {
            id: 4,
            name: 'Card Tables',
            listings: []
        }, {
            id: 5,
            name: 'Nest of Tables',
            listings: []
        }, {
            id: 6,
            name: 'Console Tables',
            listings: []
        }, {
            id: 7,
            name: 'Game Tables',
            listings: []
        }, {
            id: 8,
            name: 'Side Tables',
            listings: []
        }]
    };

    desks = {
        id: 2,
        name: 'Desks',
        children: [{
            id: 1,
            name: 'Desk',
            listings: []
        }, {
            id: 2,
            name: 'Library Tables',
            listings: []
        }, {
            id: 3,
            name: 'Writing Tables',
            listings: []
        }]
    };

    bookcases = {
        id: 3,
        name: 'Bookcases',
        children: []
    };

    furniture = {
        id: 1,
        name: 'Furniture',
        children: [tables, desks, bookcases]
    };

    /**
     * End Furniture
     */

    /**
     * Silver
     */

    candle = {
        id: 2,
        name: 'Candle Sticks',
        children: [{
            id: 1,
            name: 'Candle Sticks',
            listings: []
        }, {
            id: 2,
            name: 'Candlebras',
            listings: []
        }]
    };

    centerpieces = {
        id: 3,
        name: 'Center Pieces',
        children: []
    };

    silver = {
        id: 1,
        name: 'Silver',
        children: [candle, centerpieces]
    };

    clocks = {
        id: 1,
        name: 'Clocks',
        children: []
    };

    /**
     * End Silver
     */

    categoryFixtureData = [
        furniture, silver, clocks
    ];

    function Categories($q) {
        var categories = {};

        categories.getList = function () {
            return $q.when(categoryFixtureData);
        };

        return categories;
    }

    angular.module('api.categories', [])
        .factory('Categories', Categories);
}());