(function () {
    'use strict';

    angular.module('ogled', [
        'ui.router',
        'ui.bootstrap',
        'templates',
        'api',
        'app.cart',
        'app.common',
        'app.home',
        'app.navbar',
        'app.routes',
        'app.sidenav',
        'app.listings',
        'app.account'
    ]);

}());