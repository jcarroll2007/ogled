(function () {
    'use strict';

    function Auth($http, $rootScope, $q, url) {
        var auth = {};
        auth._events = $rootScope.$new();

        auth._currentUser = {};
        auth.getCurrentUser = function () {
            if (auth._currentUserPromise) {
                return auth._currentUserPromise;
            }
            if (auth._currentUser.id) {
                auth._currentUserPromise = $q.when(auth._currentUser);
                return auth._currentUserPromise;
            }

            // Fix this to get the current user from the backend
            return $q.reject();
        };

        function onSuccessfulLogin(response) {
            auth._currentUser = response.data;
            // Do not return the user so that anyone listening must use
            // the getCurrentUser method
            auth._events.$broadcast('login');
        }
        function onLoginFailed(message) {
            auth._events.$broadcast('loginFailed', message);
        }
        auth.login = function (email, password) {
            return $http.post(url('login'), {username: email, password: password})
                .then(onSuccessfulLogin, onLoginFailed);
        };

        auth.logout = function () {
            return $http.get(url('logout'))
                .then(function () {
                    auth.reset();
                    auth._events.$broadcast('logout');
                });
        };

        auth.reset = function () {
            auth._currentUser = {};
            auth._currentUserPromise = undefined;
        };

        function validate(user) {
            var successful = true,
                reasonsForFailure = [];

            if (!user.email) {
                successful = false;
                reasonsForFailure.push('Email address is invalid.');
            }
            if (!user.password) {
                successful = false;
                reasonsForFailure.push('No password.');
            }
            if (user.password !== user.password_verification) {
                successful = false;
                reasonsForFailure.push('Passwords do not match.');
            }
            if (!user.first_name) {
                successful = false;
                reasonsForFailure.push('No first name.');
            }
            if (!user.last_name) {
                successful = false;
                reasonsForFailure.push('No last name.');
            }

            return {
                successful: successful,
                reasonsForFailure: reasonsForFailure
            };
        }
        auth.register = function (newUser) {
            var validation = validate(newUser);
            if (validation.successful) {
                delete newUser.password_verification;
                return $http.post(url('register'), newUser);
            }

            return $q.reject(validation.reasonsForFailure);
        };

        /*jslint unparam: true */
        auth.on = function (event, callback) {
            auth._events.$on(event, function (event, data) {
                callback(data);
            });
        };

        return auth;
    }

    angular.module('common.auth', [])
        .factory('Auth', Auth);
}());