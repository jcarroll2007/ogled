(function () {
    'use strict';

    function ListingController(Cart) {
        this.addToCart = Cart.addItem;
    }

    function listing() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'common/listing/listing.html',
            controller: ListingController,
            controllerAs: 'listing',
            scope: {},
            bindToController: {
                ngModel: '='
            }
        };
    }

    angular.module('common.listing', ['app.cart'])
        .directive('listing', listing);
}());