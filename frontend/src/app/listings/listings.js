(function () {
    'use strict';

    function ListingsController() {
        var ctrl = this;

        ctrl.filter = function (selectedPeriods) {
            _.forEach(ctrl.ngModel, function (listing) {
                listing.isActive = false;
            });
            _.forEach(selectedPeriods, function (period) {
                _.forEach(ctrl.ngModel, function (listing) {
                    if (listing.period === period.id) {
                        listing.isActive = true;
                        return;
                    }
                });
            });
        };
    }

    function listings() {
        return {
            restrict: 'E',
            templateUrl: 'app/listings/listings.html',
            controller: ListingsController,
            controllerAs: 'listings',
            scope: {},
            bindToController: {
                ngModel: '='
            }
        };
    }

    angular.module('app.listings', ['app.listings.periods'])
        .directive('listings', listings);
}());