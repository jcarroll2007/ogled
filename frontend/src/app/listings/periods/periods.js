(function () {
    'use strict';

    function PeriodsDropdownController(Periods) {
        var ctrl = this;

        ctrl.selectedChanged = function () {
            ctrl.onSelectionUpdated({selectedPeriods: ctrl.selected});
        };

        // ctrl.toggleAllSelected = function (selectAll) {
        //     ctrl.allSelected = selectAll || !ctrl.allSelected;
        //     _.forEach(ctrl.periods, function (period) {
        //         period.isSelected = ctrl.allSelected;
        //     });
        //     ctrl.selected = ctrl.allSelected ? ctrl.periods.slice() : [];
        //     ctrl.selectedChanged();
        // };

        // ctrl.toggleSelected = function (period) {
        //     var index = _.indexOf(ctrl.selected, period);
        //     if (index !== -1) {
        //         ctrl.selected.splice(index, 1);
        //         period.isSelected = false;
        //     } else {
        //         ctrl.selected.push(period);
        //         period.isSelected = true;
        //     }
        //     ctrl.selectedChanged();
        // };

        ctrl.selectAll = function () {
            ctrl.selected = ctrl.periods;
            ctrl.allSelected = true;
            ctrl.active = "all";
            ctrl.selectedChanged();
        };
        ctrl.select = function (period) {
            ctrl.selected = [period];
            ctrl.allSelected = false;
            ctrl.active = period.id;
            ctrl.selectedChanged();
        };

        Periods.getList().then(function (periods) {
            ctrl.periods = periods;
            ctrl.selectAll();
            ctrl.allSelected = true;
            // ctrl.toggleAllSelected(true);
        });
    }

    function periodsDropdown() {
        return {
            templateUrl: 'app/listings/periods/periods.html',
            restrict: 'E',
            controller: PeriodsDropdownController,
            controllerAs: 'periods',
            scope: {},
            bindToController: {
                selected: '=',
                onSelectionUpdated: '&'
            }
        };
    }

    angular.module('app.listings.periods', [])
        .directive('periodsDropdown', periodsDropdown);
}());