(function () {
    'use strict';

    function NavbarController(Sidenav, Cart) {
        this.toggleSidenav = Sidenav.toggle;

        this.toggleCart = Cart.toggle;
    }

    function navbar() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/navbar/navbar.html',
            controller: NavbarController,
            controllerAs: 'navbar'
        };
    }

    angular.module('app.navbar', ['ui.router', 'app.navbar.login'])
        .directive('navbar', navbar);
}());