(function () {
    'use strict';

    function LoginDropdownController(Auth, $timeout) {
        var ctrl = this;
        ctrl.expanded = false;
        ctrl.registering = false;

        ctrl.toggle = function () {
            ctrl.expanded = !ctrl.expanded;
            if (!ctrl.expanded && ctrl.registering) {
                ctrl.registering = false;
            }
            $timeout(function () {
                $('#login-dropdown-email').focus();
            });
        };

        ctrl.toggleRegister = function () {
            ctrl.registering = !ctrl.registering;
            ctrl.loginError = false;
        };

        ctrl.login = function () {
            if (ctrl.loggingIn) {
                return ctrl.loggingIn;
            }
            ctrl.loggingIn = Auth.login(ctrl.email, ctrl.password)
                .then(function () {
                    ctrl.loggingIn = undefined;
                });
            return ctrl.loggingIn;
        };
        function onLogin() {
            ctrl.reset();
            Auth.getCurrentUser()
                .then(function (user) {
                    ctrl.currentUser = user;
                });
        }
        Auth.on('login', onLogin);
        Auth.on('loginFailed', function () {
            ctrl.loginError = true;
        });

        ctrl.logout = function () {
            if (ctrl.loggingOut) {
                return ctrl.loggingOut;
            }
            ctrl.loggingOut = Auth.logout()
                .then(function () {
                    ctrl.loggingOut = undefined;
                });
            return ctrl.loggingOut;
        };
        function onLogout() {
            ctrl.currentUser = undefined;
            ctrl.expanded = false;
        }
        Auth.on('logout', onLogout);

        ctrl.register = function () {
            if (ctrl.registerPromise) {
                return ctrl.registerPromise;
            }
            var newUser = {
                first_name: ctrl.first_name,
                last_name: ctrl.last_name,
                email: ctrl.email,
                password: ctrl.password,
                password_verification: ctrl.password_verification
            };
            ctrl.registerPromise = Auth.register(newUser)
                .then(ctrl.login)
                .then(function () {
                    ctrl.registerPromise = undefined;
                });
            return ctrl.registerPromise;
        };

        ctrl.reset = function () {
            ctrl.email = '';
            ctrl.password = '';
            ctrl.password_verification = '';
            ctrl.first_name = '';
            ctrl.last_name = '';
            ctrl.registering = false;
            ctrl.loginError = false;
        };

        // Listen for clicks outside of the dropdown
        // $(document).bind('click', function (event) {
        //     var isChild = $element
        //         .find(event.target)
        //         .length > 0;
        //     console.log('test');

        //     if (isChild) {
        //         return;
        //     }

        //     ctrl.expanded = false;
        // });

    }

    function loginDropdown() {
        return {
            restrict: 'E',
            templateUrl: 'app/navbar/login/login.html',
            controller: LoginDropdownController,
            controllerAs: 'loginDropdown'
        };
    }


    angular.module('app.navbar.login', ['api', 'common.auth'])
        .directive('loginDropdown', loginDropdown);
}());