(function () {
    'use strict';

    function stateConfig($stateProvider) {
        $stateProvider
            .state('app.account', {
                url: '/account',
                template: '<ui-view/>',
                abstract: true,
                resolve: {
                    currentUser: function (Auth) {
                        return Auth.login('jcarroll@email.com', '1234')
                            .then(function () {
                                return Auth.getCurrentUser();
                            });
                    }
                },
                controller: function (currentUser) {
                    this.currentUser = currentUser;
                },
                controllerAs: 'account'
            })
            .state('app.account.overview', {
                url: '/overview',
                templateUrl: 'app/account/overview/overview.html'

            });
    }

    angular.module('app.account', [])
        .config(stateConfig);
}());