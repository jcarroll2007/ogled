(function () {
    'use strict';

    function Cart() {
        var service = this;

        service.reset = function () {
            service._items = [];
            service.collapsed = true;
        };
        service.reset();

        service.toggle = function () {
            service.collapsed = !service.collapsed;
        };

        service.getState = function () {
            return service.collapsed;
        };

        service.addItem = function (item) {
            service._items.push(item);
        };

        service.getItems = function () {
            return service._items;
        };

        return service;
    }

    function CartController(Cart) {
        this.isCollapsed = Cart.getState;

        this.items = Cart.getItems();
    }

    function cart() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/cart/cart.html',
            controller: CartController,
            controllerAs: 'cart'
        };
    }

    angular.module('app.cart', [])
        .directive('cart', cart)
        .service('Cart', Cart);
}());