(function () {
    'use strict';

    function HomeController(Listings) {
        var ctrl = this;

        Listings.getList()
            .then(function (listings) {
                ctrl.listings = listings;
            });
    }

    function home() {
        return {
            restrict: 'E',
            templateUrl: 'app/home/home.html',
            controller: HomeController,
            controllerAs: 'home'
        };
    }

    angular.module('app.home', ['api'])
        .directive('home', home);
}());