(function () {
    'use strict';

    function Sidenav() {
        var collapsed = false;
        this.toggle = function () {
            collapsed = !collapsed;
        };

        this.getState = function () {
            return collapsed;
        };

        return this;
    }

    function SidenavController(Sidenav, Categories, Groups, $state) {
        var ctrl = this;
        ctrl.isCollapsed = Sidenav.getState;
        Categories.getList()
            .then(function (categories) {
                ctrl.categories = categories;
            });
        Groups.get()
            .then(function (groups) {
                ctrl.groups = groups.data;
            });

        ctrl.view = function ($event, group, category, sub_category) {
            ctrl.changeActive($event.currentTarget);
            var params = {};
            if (group) {
                params.group = group.id;
            }
            if (category) {
                params.category = category.id;
            }
            if (sub_category) {
                params.sub_category = sub_category.id;
            }
            $state.go('app.listings', params, { inherit: false });
        };

        ctrl.changeActive = function (element) {
            window.elementSelected = element;
            if (ctrl.previousSelection) {
                ctrl.previousSelection.classList.remove('active');
            }

            element.classList.add('active');
            ctrl.previousSelection = element;
        };
    }

    function sidenav() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/sidenav/sidenav.html',
            controller: SidenavController,
            controllerAs: 'sidenav'
        };
    }

    angular.module('app.sidenav', ['app.sidenav.dropdown', 'api'])
        .directive('sidenav', sidenav)
        .service('Sidenav', Sidenav);
}());