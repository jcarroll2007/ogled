(function () {
    'use strict';

    function SidenavDropdownController() {
        this.expanded = false;
        this.toggle = function () {
            this.expanded = !this.expanded;
        };
        this.titleClicked = function ($event) {
            this.onClick({$event: $event});
        };
    }

    function sidenavDropdown() {
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'app/sidenav/dropdown/dropdown.html',
            controller: SidenavDropdownController,
            controllerAs: 'sidenavDropdown',
            bindToController: true,
            scope: {
                'title': '@',
                'onClick': '&'
            }
        };
    }

    angular.module('app.sidenav.dropdown', [])
        .directive('sidenavDropdown', sidenavDropdown);
}());