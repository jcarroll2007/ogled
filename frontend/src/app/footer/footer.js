(function () {
    'use strict';

    function footer() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/footer/footer.html'
        };
    }

    angular.module('app.footer', [])
        .directive('footer', footer);
}());