# Loading Fixtures

Run the following commands to load the fixture data for each app.

## api
`python manage.py loaddata --app api api/fixtures/initial_data.json`